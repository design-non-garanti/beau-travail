# beau-travail alpha (0.12)

- [0.1] scales 
  - [0.2] recursive scales
  - [0.3] input and output scales (screen)
    - [ ] camera
- [0.4] floodfill 
  - [0.5] parametric floodfill
  - [0.6] data floodfil
  - [0.7] multidimentional floodfill
- [0.8] mobile objects 
  - [0.9] keys to move
  - [0.10] keys to events
    - [0.13] floodfill events
  - [0.11] flood to mobiles 
    - [0.12] flood data to mobile
    - [ ] data inventory
    - [ ] data effects
    - [ ] craftable datas
      - [ ] customizable floodfill events
- [ ] goal / narrative

## gameplay

- [ ] shoot them all aspects
  - [ ] real time
  - [ ] waves of enemies 
  <!-- - [ ] procedurals movings levels (?)-->
  - [ ] parametric, upgradable, objets (weapon, heal)
  - [ ] coop

- [ ] levels
  - [ ] generative environnements (wall, toxic, ice, nothing, lava, water …)
  - [ ] generative mazes (rooms, corridors) 
  - [ ] on each level you have to find the stairs for the next level wihout dying
  

<!--
- [ ] survival RPG aspect
  - [ ] editable levels
  - [ ] craftable upgrades
  -->