Mobile = function(options){
	var mobile=this;
	define(this,options,{
		name:Math.random(),
		class:'mobile',
		jump:1,
		capacity:10,
		vacuum:false,
		floodFill:{scope:solids,lifeTime:5,owner:mobile,dir:floodFills['default']['dir']},
		keys:{},
		inventory:[],
	});
	this.datas=datas[this.class];
	this.eventsKeys=eventsKeys[this.class];
	onKeyDownEvents.push(function(e){
		mobile.events(e,true);
	})
	onKeyUpEvents.push(function(e){
		mobile.events(e,false);
	})
	this.position.fill({data:this.class});
	this.position.mobiles[this.name]=this;
}

Mobile.prototype.events = function(e,bool) {
	if(this.eventsKeys[e.key])this.keys[e.key]=bool;
	for(var i in this.keys){
		if(this.keys[i])this.eventsKeys[i](this);
	}
}

Mobile.prototype.move = function(dir){
	if(this.dead)return false
		var relayDir=[];
		for(var i in dir){
			relayDir.push(dir[i]*this.jump);
		}
	relay = this.position.findRelay(relayDir);
	this.floodFill.data=this.class;
	this.floodFill.bool=true;
	this.floodFill.id=false;

	if(relay && relay.fill(this.floodFill)){
		animations['jump'](this,[dir]);
		this.floodFill.bool=false;
		this.floodFill.id=false;
		this.floodFill.scope=[];
		this.position.fill(this.floodFill);
		this.floodFill.scope=solids;
		this.position.mobiles[this.name]=false;
		this.position=relay;
		this.position.mobiles[this.name]=this;
	}

}

