Scale = function(options){
	define(this,options,{
		datas:{},
		context:[],
		floods:{},
		mobiles:{},
		domOutput:false,
		input:false,
	});
	this.context=[this].concat(this.context);
	this.datas[this.class]=true;
	if(this.dom)this.createDOM();
}

Scale.prototype.buildIn = function(howmany,what){
	if(!this[what.class])this[what.class]=[];
	what.context=this.context;
	what.position=what.context[0][what.class].length;
	this[what.class].push(new Scale(what));
	howmany--;
	if(!howmany)return true;
	return this.buildIn(howmany,what);
}

Scale.prototype.fill = function(options,path){
	if(!options.parsed)options=parseFloodFill(options);
	if(!options.id)options.id=Math.random();
	if(!floodEnding(this,options,options.floodEnd))return false
	//if(options.bool)floodMobiles(options,this.mobiles,path);
	this.floods[options.id]=true;
	if(options.data)this.datas[options.data]=options.bool;
	if(this.dom)this.updateDOM();
	if(this.domOutput)this.domOutput.updateDOM();
	return true
}

Scale.prototype.floodFill = function(options,path){
	if(!path)path=[];
	if(!options.parsed)options=parseFloodFill(options);
	if(!this.fill(options,path))return false
		options.positions.push(this);
	options.lifeTime--;
	var relays=[];
	for(var d in options.dir)if(this.findRelay(options.dir[d]))relays.push(this.findRelay(options.dir[d]));
		setTimeout(function(){
			for(var r in relays){var new_path=path.concat([relays[r]]);relays[r].floodFill(options,new_path)}
		},options.speed);
	return options
}

Scale.prototype.findRelay = function(dir,dim,position){
	if(!dim)dim=0;
	if(dim>dir.length-1)return position
		var parent = this.context[this.context.length-dim-1][this.context[this.context.length-dim-2].class][dir[dir.length-dim-1]+this.context[this.context.length-dim-2].position];
	if(parent){
		if(!position){
			position=parent;
		}else{
			position=position[this.context[this.context.length-dim-2].class][dir[dir.length-dim-1]+this.context[this.context.length-dim-2].position];
		}
	}else{
		return false
	}
	dim++;
	return this.findRelay(dir,dim,position)
}

Scale.prototype.log= function(log,delay,pt,i){
	if(!pt)pt=[0,0,0];
	if(!i){
		i=0;scale=this.findRelay([1+pt[0],0+pt[1],0+pt[2]])
	}else{
		scale=this;
	}
	if(!delay)delay=10000;
	if(i>log.length)return true;
	animation({floodEnd:floodEnds['log'],position:scale,data:log[i],lifeTime:1,delay:delay,id:'log'});
	i++;
	setTimeout(function(){
		if(scale.findRelay([1,0,0]))scale.findRelay([1,0,0]).log(log,delay,pt,i);
	},17);
}