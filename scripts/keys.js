var onKeyDownEvents=[];
var onKeyUpEvents=[];

document.onkeydown=function(e){
	for(var i in onKeyDownEvents){
		onKeyDownEvents[i](e);
	}
}

document.onkeyup=function(e){
	for(var i in onKeyUpEvents){
		onKeyUpEvents[i](e);
	}
}